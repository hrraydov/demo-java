package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	private PostService postService;

	@Test
	public void test() {
		TextPost post = new TextPost();
		post.setTitle("text post");
		post.setContent("content");
		ImagePost post2 = new ImagePost();
		post2.setTitle("image post");
		post2.setUrl("url example");
		postService.create(post);
		postService.create(post2);
		System.err.println(postService.getAll());
	}

}
