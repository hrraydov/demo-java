package com.example;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;

@Entity
@DiscriminatorValue(value = "image")
@Data
public class ImagePost extends Post {
	private String url;
	
	@Override
	public String toString() {
		return "Image [id=" + id + ", title=" + title + ", url=" + url + "]";
	}
}
