package com.example;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@DiscriminatorValue(value = "text")
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class TextPost extends Post {
	private String content;

	@Override
	public String toString() {
		return "TextPost [id=" + id + ", title=" + title + ", content=" + content + "]";
	}
}
