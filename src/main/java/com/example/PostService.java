package com.example;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PostService {

	@Autowired
	private PostRepository postRepository;

	public List<Post> getAll() {
		return postRepository.findAll();
	}

	public Post create(ImagePost post) {
		return postRepository.save(post);
	}

	public Post create(TextPost post) {
		return postRepository.save(post);
	}

}
